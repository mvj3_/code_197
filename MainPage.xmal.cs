using NotificationsExtensions.TileContent;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Windows.Foundation;
using NotificationsExtensions.TileContent;
using Windows.UI.Notifications;

using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// “空白页”项模板在 http://go.microsoft.com/fwlink/?LinkId=234238 上有介绍

namespace tie
{
    /// <summary>
    /// 可用于自身或导航至 Frame 内部的空白页。
    /// </summary>
    public sealed partial class MainPage : Page
    {
        public MainPage()
        {
            this.InitializeComponent();
        }

        /// <summary>
        /// 在此页将要在 Frame 中显示时进行调用。
        /// </summary>
        /// <param name="e">描述如何访问此页的事件数据。Parameter
        /// 属性通常用于配置页。</param>
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            
            
                
           
        }
        private void tiles(string pic, int i) {
            NotificationsExtensions.TileContent.ITileWideImageAndText01 tileContent = TileContentFactory.CreateTileWideImageAndText01();
            tileContent.TextCaptionWrap.Text = i.ToString();
            tileContent.Image.Src = pic;
            tileContent.Image.Alt = i.ToString();


            // create the square template and attach it to the wide template
            ITileSquareImage squareContent = TileContentFactory.CreateTileSquareImage();
            squareContent.Image.Src = pic;
            squareContent.Image.Alt = i.ToString();
            tileContent.SquareContent = squareContent;

            // Send the notification to the app's application tile.
            TileUpdateManager.CreateTileUpdaterForApplication().Update(tileContent.CreateNotification());
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
           
        }

        private void Button_Click_Queue_on(object sender, RoutedEventArgs e)
        {
            //开启磁贴滚动
            TileUpdateManager.CreateTileUpdaterForApplication().EnableNotificationQueue(true);
        }

        private void Button_Click_Queue_off(object sender, RoutedEventArgs e)
        {
            //开启磁贴滚动
            TileUpdateManager.CreateTileUpdaterForApplication().EnableNotificationQueue(false);
        }

        private void Button_Click_clear(object sender, RoutedEventArgs e)
        {
            TileUpdateManager.CreateTileUpdaterForApplication().Clear();
        }

        private void Button_Click_create(object sender, RoutedEventArgs e)
        {
            Windows.UI.Notifications.TileUpdateManager.CreateTileUpdaterForApplication().Clear();


            string[] picArray = { "Assets/Image/1.png", "Assets/Image/2.png", "Assets/Image/3.png", "Assets/Image/4.png" };
            int i = 1;
            foreach (var pic in picArray)
            {
                this.tiles(pic, i);
                i++;
            }
        }
    }
}
